# DNS Check

Python script to emulate https://dnsblacklist.org via command line.

### Run
```
python3 main.py <URL to check>
```

### Example

([Source](https://nitter.net/unmaskparasites/status/1303792031693778947#m))
```
python3 main.py solo.declarebusinessgroup.ga # malware domain
```

### Output
```
Resolver                       IP Address    Time
-------------------------------------------------
Localhost                   No Resolution    74ms
114DNS                       45.9.148.228   275ms
AdGuard                      45.9.148.228    24ms
AdGuard Family Protection    45.9.148.228    17ms
AdGuard Unfiltered           45.9.148.228    20ms
Adhole (US)                  45.9.148.228   166ms
AliDNS                       45.9.148.228   402ms
Alternate DNS                45.9.148.228   163ms
Baidu                        45.9.148.228  1770ms
CleanBrowsing Adult         No Resolution    24ms
CleanBrowsing Family        No Resolution    18ms
CleanBrowsing Security      No Resolution    18ms
CloudFlare                   45.9.148.228    19ms
CloudFlare Family            45.9.148.228    26ms
CloudFlare Security          45.9.148.228    20ms
Comodo Secure                45.9.148.228    17ms
CZ.NIC                       45.9.148.228   182ms
DNSpai                       45.9.148.228   624ms
DNS.WATCH                    45.9.148.228   192ms
Dyn                          45.9.148.228   334ms
FDN                          45.9.148.228   194ms
FreeDNS (US)                 45.9.148.228   185ms
Freenom                      45.9.148.228   303ms
Google                       45.9.148.228    25ms
Hurricane Electric           45.9.148.228   365ms
Level3                       45.9.148.228    44ms
Mullvad                      45.9.148.228   524ms
Neustar Unfiltered           45.9.148.228    78ms
Neustar Family Secure        45.9.148.228   325ms
Neustar Threat Protection    45.9.148.228    77ms
NextDNS                      45.9.148.228   269ms
NixNet (Adblock)             45.9.148.228    63ms
NordVPN                      45.9.148.228    85ms
OneDNS                       45.9.148.228   967ms
OpenDNS                      45.9.148.228   153ms
OpenDNS Family               45.9.148.228   223ms
puntCAT                      45.9.148.228   269ms
Quad9                       No Resolution    81ms
SafeDNS                      45.9.148.228   163ms
SkyDNS                       45.9.148.228   205ms
Uncensored DNS               45.9.148.228   182ms
Verisign                     45.9.148.228    73ms
Yandex Basic                 45.9.148.228   293ms
Yandex Family                45.9.148.228   211ms
Yandex Safe                  45.9.148.228   281ms
```