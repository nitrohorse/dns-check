import sys
import subprocess
from timeit import default_timer as timer

if len(sys.argv) == 1:
	print('Please specify a URL to check...')
	sys.exit()

url = sys.argv[1]

resolvers = {
	'Localhost': '127.0.0.1',
	'114DNS': '114.114.114.114', # https://www.114dns.com/
	'AdGuard': '94.140.14.14', # https://adguard.com/en/adguard-dns/overview.html
	'AdGuard Family Protection': '94.140.14.15', # https://adguard.com/en/adguard-dns/overview.html
	'AdGuard Unfiltered': '94.140.14.140', # https://adguard.com/en/adguard-dns/overview.html
	'Adhole (US)': '63.142.251.101', # https://adhole.org/
	'AliDNS': '223.5.5.5', # http://www.alidns.com/
	'Alternate DNS': '23.253.163.53', # https://alternate-dns.com/
	'Baidu': '180.76.76.76', # https://www.publicdns.xyz/public/baidu.html
	'CleanBrowsing Adult': '185.228.168.10', # https://cleanbrowsing.org/
	'CleanBrowsing Family': '185.228.168.168', # https://cleanbrowsing.org/
	'CleanBrowsing Security': '185.228.168.9', # https://cleanbrowsing.org/
	'CloudFlare': '1.1.1.1', # https://1.1.1.1/dns/
	'CloudFlare Family': '1.1.1.3', # https://1.1.1.1/family/
	'CloudFlare Security': '1.1.1.2', # https://1.1.1.1/family/
	'Comodo Secure': '8.26.56.26', # https://www.comodo.com/secure-dns/
	'CZ.NIC': '193.17.47.1', # https://www.nic.cz/odvr/
	'DNSpai': '101.226.4.6', # http://www.dnspai.com/public.html
	'DNS.WATCH': '84.200.69.80', # https://dns.watch/
	'Dyn': '216.146.35.35', # https://help.dyn.com/internet-guide-setup/
	'FDN': '80.67.169.12', # https://www.fdn.fr/actions/dns/
	'FreeDNS (US)': '45.33.97.5', # https://freedns.zone/en/
	'Freenom': '80.80.81.81', # https://www.freenom.world/
	'Google': '8.8.8.8', # https://developers.google.com/speed/public-dns/
	'Hurricane Electric': '74.82.42.42', # https://bgp.he.net/net/74.82.0.0/18
	'Level3': '209.244.0.3', # https://www.publicdns.xyz/public/level3.html
	'Mullvad': '193.138.218.74', # https://mullvad.net/en/help/dns-leaks/
	'Neustar Unfiltered': '156.154.70.5', # https://www.publicdns.neustar/
	'Neustar Family Secure': '156.154.70.3', # https://www.publicdns.neustar/
	'Neustar Threat Protection': '156.154.70.2', # https://www.publicdns.neustar/
	'NextDNS': '45.90.28.0', # https://bash.ws/whois/45.90.28.0
	'NixNet (Adblock)': '198.251.90.89', # https://docs.nixnet.services/NixNet_DNS
	'Njalla': '95.215.19.53', # https://dns.njal.la
	'NordVPN': '103.86.96.100', # https://support.nordvpn.com/Other/1047409702/What-are-your-DNS-server-addresses.htm
	'OneDNS': '117.50.11.11', # https://www.onedns.net/
	'OpenDNS': '208.67.222.222', # https://use.opendns.com/
	'OpenDNS Family': '208.67.222.123', # https://use.opendns.com/
	'puntCAT': '109.69.8.51', # http://www.servidordenoms.cat/
	'Quad9': '9.9.9.9', # https://www.quad9.net/
	'SafeDNS': '195.46.39.39', # https://www.safedns.com/
	'SkyDNS': '193.58.251.251', # https://www.skydns.ru/
	'Uncensored DNS': '91.239.100.100', # https://blog.uncensoreddns.org/
	'Verisign': '64.6.64.6', # https://www.verisign.com/en_US/security-services/public-dns/index.xhtml
	'Yandex Basic': '77.88.8.8', # https://dns.yandex.com/
	'Yandex Family': '77.88.8.7', # https://dns.yandex.com/
	'Yandex Safe': '77.88.8.88' # https://dns.yandex.com/
}

print(f"{'Resolver':<25} {'IP Address':>15} {'Time':>7}")
print('-------------------------------------------------')
for resolver in resolvers:
	cmd = 'dig +tries=1 +time=3 +short ' + url + ' @' + resolvers[resolver] + ' | tail -n1' # get first IP address
	output = ''
	time = '0'

	start = timer()
	output = subprocess.run(
		cmd,
		shell = True,
		capture_output = True,
		text = True
	).stdout
	end = timer()
	time = str((end - start) * 1000).split('.')[0]
	output = output.replace('\n', '')

	# Alternate DNS block: 198.101.242.72
	# AdGuard block: 94.140.14.33, 94.140.14.35
	# Neustar block: 156.154.113.17
	# SafeDNS block: 195.46.39.1
	# OpenDNS block: 146.112.61.108, 146.112.61.106
	# Yandex block: 93.158.134.250
	if not output or \
		output == '0.0.0.0' or \
		output == '198.101.242.72' or \
		'94.140.14.' in output or \
		output == '156.154.113.17' or \
		output == '195.46.39.1' or \
		output == '93.158.134.250' or \
		'146.112.61.' in output:
		output = 'No Resolution'
	elif 'timed out' in output:
		output = 'Timeout'

	print(f'{resolver:<25} {output:>15} {time:>5}ms')
